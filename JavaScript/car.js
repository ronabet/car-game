export class Car
{
    constructor(canvas, image, x, y, dy) 
    {
        this.dyCar = dy;
        this.xCar= x;
        this.yCar=y;
        this.image=image;
        this.canvas = canvas;
    }
    drawCar(ctx)
    {
        ctx.drawImage(this.image, this.xCar, this.yCar);
    }

    move()
    {
        var randomNum  = (Math.random() * 4) + 1;
        if(this.yCar > this.canvas.height + 150) //move the cars
        {
            this.yCar = -200;
            this.dyCar = randomNum;
        }
    this.yCar += this.dyCar;
    }
}
