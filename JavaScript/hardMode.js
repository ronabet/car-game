import {Car} from './hardModeCar.js';

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var yCarFirst = -200;
var dyCarFirst = 3;
var paddleHeight = 149;
var paddleWidth = 75;
var paddleX = (canvas.width-paddleWidth)/2;
var paddleY = (canvas.height-paddleHeight);
var rightPressed = false;
var leftPressed = false;
var upPressed = false;
var downPressed = false;
var randomNum  = Math.floor((Math.random() * 180) + 1);

var buggati = new Image();
buggati.src = "./images/buggati.png";
var passat = new Image();
passat.src = "./images/audir8.png";
var audi = new Image();
audi.src = "./images/mclaren.png";
var arona = new Image();
arona.src = "./images/ferrari.png";
var car = new Image();
car.src= "./images/car.png";


var audiCar = new Car(canvas, audi, -10,yCarFirst - randomNum, 5);
var passatCar = new Car(canvas, passat, 330,yCarFirst - randomNum, 6);
var mazdaCar = new Car(canvas, buggati, 150, yCarFirst - randomNum, 10);
var aronaCar = new Car(canvas, arona, 483, yCarFirst - randomNum, 3);

var background = new Image();
background.src = "./images/traffic2.jpg";


document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

function keyDownHandler(e) 
{
    if(e.key == "Right" || e.key == "ArrowRight") 
    {
        rightPressed = true;
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") 
    {
        console.log('change left');
        leftPressed = true;
    }else if(e.key == "Up" || e.key == "ArrowUp")
    {
        console.log('change up');
        
        upPressed = true;
    } else if(e.key == "Down" || e.key == "ArrowDown")
    {
        console.log('change down');
        downPressed = true;
    }
}

function keyUpHandler(e) 
{
    if(e.key == "Right" || e.key == "ArrowRight") 
    {
        rightPressed = false;
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") 
    {
        leftPressed = false;
    } else if(e.key == "Up" || e.key == "ArrowUp")
    {
        upPressed = false;
    } else if(e.key == "Down" || e.key == "ArrowDown")
    {
        downPressed = false;
    }
}

// function drawBall() 
// {
//     ctx.drawImage(audi, xCarFirst, yCarFirst);
// }

function drawPaddle() 
{
    ctx.drawImage(car, paddleX , paddleY);
}

function collisionDetection(paddleX, paddleY, carY,carX)
{
    if(paddleX == carY && paddleY == carX )
    {
        alert("game over");
    }
    else if(paddleX == carX || paddleY == carY)
    {
        alert("game over");
    }

}
function draw() 
{
    ctx.drawImage(background, 0, 0); // set background on the canvas
    drawPaddle();
    mazdaCar.drawCar(ctx);
    mazdaCar.move();
    aronaCar.drawCar(ctx);
    aronaCar.move();
    passatCar.drawCar(ctx);
    passatCar.move();
    audiCar.drawCar(ctx);
    audiCar.move();
    // if(yCarFirst > canvas.height + 150) //move the cars
    // {
    //     yCarFirst = -200;
    //     dyCarFirst = 2;
    // }
    //collisionDetection(paddleX, paddleY, yCarFirst,140);
    
    if(rightPressed && paddleX < canvas.width-paddleWidth) //car left and right
    {
        
        paddleX += 9;
    }
    else if(leftPressed && paddleX > 0) 
    {
        paddleX -= 9;
    } 
    else if (upPressed && paddleY > 0)
    { 
        paddleY -= 9;
    } 
    else if (downPressed && paddleY < canvas.height - paddleHeight)
    { 
        paddleY += 9;
    }
    yCarFirst += dyCarFirst;
}

setInterval(draw, 10);
