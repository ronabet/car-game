import {Car} from './car.js';

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
//var xCarFirst = 10;
var yCarFirst = -200;
var dyCarFirst = 3;
var paddleHeight = 149;
var paddleWidth = 73;
var paddleX = (canvas.width-paddleWidth)/2;
var paddleY = (canvas.height-paddleHeight);
var rightPressed = false;
var leftPressed = false;
var upPressed = false;
var downPressed = false;
var randomNum  = Math.floor((Math.random() * 180) + 1);

var mazda = new Image();
mazda.src = "./images/mazda.png";
var passat = new Image();
passat.src = "./images/passat.png";
var audi = new Image();
audi.src = "./images/audi.png";
var arona = new Image();
arona.src = "./images/arona.png";
var car = new Image();
car.src= "./images/car.png";


var audiCar = new Car(canvas, audi, 10,yCarFirst - randomNum, 1);
var passatCar = new Car(canvas, passat, 330,yCarFirst - randomNum, 1);
var mazdaCar = new Car(canvas, mazda, 140, yCarFirst - randomNum, 0.55555);
var aronaCar = new Car(canvas, arona, 460, yCarFirst - randomNum, 1);

var background = new Image();
background.src = "./images/traffic2.jpg";


document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

function keyDownHandler(e) 
{
    if(e.key == "Right" || e.key == "ArrowRight") 
    {
        rightPressed = true;
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") 
    {
        console.log('change left');
        leftPressed = true;
    }else if(e.key == "Up" || e.key == "ArrowUp")
    {
        console.log('change up');
        
        upPressed = true;
    } else if(e.key == "Down" || e.key == "ArrowDown")
    {
        console.log('change down');
        downPressed = true;
    }
}

function keyUpHandler(e) 
{
    if(e.key == "Right" || e.key == "ArrowRight") 
    {
        rightPressed = false;
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") 
    {
        leftPressed = false;
    } else if(e.key == "Up" || e.key == "ArrowUp")
    {
        upPressed = false;
    } else if(e.key == "Down" || e.key == "ArrowDown")
    {
        downPressed = false;
    }
}

// function drawBall() 
// {
//     ctx.drawImage(audi, xCarFirst, yCarFirst);
// }

function drawPaddle() 
{
    ctx.drawImage(car, paddleX , paddleY);
}

function collisionDetection(paddleX, paddleY, carY,carX, carWidth, carHeight)
{
    // if(paddleX > carX && paddleX < carX + carWidth && paddleY > carY && paddleY < carY +  carHeight) 
    // {
    //     alert("game over");
    // }

    if(paddleX >= carX && carX >= (canvas.width - paddleWidth) || paddleY <= carY &&  carY <= (canvas.height - paddleHeight)){
        console.log("here");
        alert("game over");
    }
}
function draw() 
{
    ctx.drawImage(background, 0, 0); // set background on the canvas
    drawPaddle();
    mazdaCar.drawCar(ctx);
    mazdaCar.move();
    aronaCar.drawCar(ctx);
    aronaCar.move();
    passatCar.drawCar(ctx);
    passatCar.move();
    audiCar.drawCar(ctx);
    audiCar.move();
    collisionDetection(paddleX, paddleY, audiCar.yCar,audiCar.yCar, 120, 149);
    // if(yCarFirst > canvas.height + 150) //move the cars
    // {
    //     yCarFirst = -200;
    //     dyCarFirst = 2;
    // }
    if(rightPressed && paddleX < canvas.width-paddleWidth) //car left and right
    {
        
        paddleX += 7;
    }
    else if(leftPressed && paddleX > 0) 
    {
        paddleX -= 7;
    } 
    else if (upPressed && paddleY > 0)
    { 
        paddleY -= 7;
    } 
    else if (downPressed && paddleY < canvas.height - paddleHeight)
    { 
        paddleY += 7;
    }
    yCarFirst += dyCarFirst;
}

setInterval(draw, 10);
